import { AppPage } from './app.po';

describe('ui-interview-client App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display title heading', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('MKP Live Data UI Client Demo');
  });
});
