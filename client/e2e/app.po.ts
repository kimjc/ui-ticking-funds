import { browser, by, element } from 'protractor';

export class AppPage {

  constructor() {
    browser.ignoreSynchronization = true;
  }
  
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.id('titletext')).getText();
  }
}
