import { TestBed, async } from '@angular/core/testing';
import { Component } from '@angular/core';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockHeaderComponent,
        MockGraphingComponent,
        MockListViewComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Mock Funds'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Mock Funds');
  }));

  // don't need below (vv) since we've made it much "nicer" with Material Design title bar.
  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  // }));
});

@Component({
  selector: 'layout-header',
  template: ''
})
class MockHeaderComponent {
}

@Component({
  selector: 'graphing-view',
  template: ''
})
class MockGraphingComponent {
}

@Component({
  selector: 'list-view',
  template: ''
})
class MockListViewComponent {
}

// describe('AppComponent', () => {
//   beforeEach(async(() => {
//       TestBed.configureTestingModule({
//         declarations: [
//           AppComponent,
//           MockNavComponent
//         ]
//       }).compileComponents();
//     }));
//
// // it(...) test cases
//
// });
//
// @Component({
//   selector: 'app-nav',
//   template: ''
// })
// class MockNavComponent {
// }
