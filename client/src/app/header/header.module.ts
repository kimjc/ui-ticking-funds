import {ModuleWithProviders, NgModule} from '@angular/core';
import {HeaderComponent} from './header.component';

import {
  MatButtonModule,
  MatToolbarModule
} from '@angular/material';

@NgModule({
  imports: [
    MatToolbarModule,
    MatButtonModule
  ],
  declarations: [
    HeaderComponent
  ],
  exports:[
    HeaderComponent
  ],
  providers: []
})
export class HeaderModule {}
