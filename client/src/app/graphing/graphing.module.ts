import {ModuleWithProviders, NgModule} from '@angular/core';
import {GraphingComponent} from './graphing.component';
import {MockDataUpdateHandler} from '../mock-data/mock-data-handler';
import {MatCardModule} from '@angular/material';

@NgModule({
  imports: [
    MatCardModule
  ],
  declarations: [
    GraphingComponent
  ],
  exports:[
    GraphingComponent
  ],
  providers: [MockDataUpdateHandler]
})
export class GraphingModule {}
