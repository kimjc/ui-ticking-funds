import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GraphingComponent } from './graphing.component';
import {MatCardModule} from '@angular/material';
import {MockDataUpdateHandler} from '../mock-data/mock-data-handler';
import {MockDataService} from '../mock-data/mock-data.service';

describe('GraphingComponent', () => {
  let component: GraphingComponent;
  let fixture: ComponentFixture<GraphingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphingComponent ],
      imports: [MatCardModule],
      providers: [MockDataUpdateHandler, MockDataService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
