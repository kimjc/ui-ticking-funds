import { Component, OnInit, ElementRef } from '@angular/core';
import {MockDataUpdateHandler} from '../mock-data/mock-data-handler';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';


@Component({
  selector: 'graphing-view',
  templateUrl: './graphing.component.html',
  styleUrls: ['./graphing.component.scss']
})
export class GraphingComponent implements OnInit {

  private initialized : boolean = false;

  constructor(private mockDataUpdateHandler : MockDataUpdateHandler) {
  }

  ngOnInit() {
    var observ = Observable.from(this.mockDataUpdateHandler.dataChange);
    observ.subscribe(
      data => {
        this.streamToChart(data);
      });
}

  streamToChart(data) {
    //console.log("graph data recvd", data);
    if (data == null || data.length == 0) return;

    var time = new Date();

    if (this.initialized != true){
      this.initialized = true;
      this.beginStreamChart(data);
    }

    var dataItems = data.map(f => {
      var result = [f.pnl]
      return result;
    });

    var traceCount = Array.from(Array(dataItems.length).keys());

    Plotly.extendTraces("chart", {
        y: dataItems
      }, traceCount);
  }

  beginStreamChart(items) {
    var data = items.map(f => {
      var result = {
        y: [f.pnl],
        type: 'scatter',
        name: f.fundName
      };
      return result;
    });

    let bgColor = '#454545';

    var layout = {
        showlegend: true,
        legend: {
          orientation: 'h',
          font: {
            family: 'Roboto',
            size: 12,
            color: 'white'
          }
        },
        paper_bgcolor: bgColor,
        plot_bgcolor: bgColor,
        xaxis: {
            title: 'Elapsed Ticks',
            titlefont: {
              family: 'Roboto',
              size: 12,
              color: 'white'
            },
            tickfont: {
              family: 'Roboto',
              size: 12,
              color: 'white'
            }
          },
          yaxis: {
            title: 'PnL',
            titlefont: {
              family: 'Roboto',
              size: 12,
              color: 'white'
            },
            tickfont: {
              family: 'Roboto',
              size: 12,
              color: 'white'
            }
          }
        };

    Plotly.newPlot("chart", data, layout, {displaylogo : false});
  }
}
