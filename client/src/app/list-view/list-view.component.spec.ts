import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListViewComponent } from './list-view.component';
import {MatTableModule} from '@angular/material';
import { Component } from '@angular/core';

import {MockDataUpdateHandler} from '../mock-data/mock-data-handler';
import {MockDataListSource} from '../mock-data/mock-data-listsource';
import {MockDataService} from '../mock-data/mock-data.service';


describe('ListViewComponent', () => {
  let component: ListViewComponent;
  let fixture: ComponentFixture<ListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListViewComponent
      ],
      imports: [MatTableModule],
      providers: [MockDataListSource, MockDataUpdateHandler, MockDataService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
