import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ListViewComponent} from './list-view.component';
import {MockDataUpdateHandler} from '../mock-data/mock-data-handler';
import {MockDataListSource} from '../mock-data/mock-data-listsource';
import {MockDataService} from '../mock-data/mock-data.service';

import {
  MatCardModule,
  MatSortModule,
  MatTableModule,
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatSortModule,
    MatTableModule
  ],
  declarations: [
    ListViewComponent
  ],
  exports:[
    ListViewComponent
  ],
  providers: [MockDataUpdateHandler, MockDataListSource, MockDataService]
})
export class ListViewModule {}
