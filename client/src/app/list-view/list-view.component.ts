import {Component, ViewChild, OnInit, OnDestroy, Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';
import {DecimalPipe} from '@angular/common';
import {VERSION} from '@angular/material';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {MatSort} from '@angular/material';

import {Subscription} from 'rxjs/Subscription';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

import {MockDataUpdateHandler} from '../mock-data/mock-data-handler';
import {MockDataListSource} from '../mock-data/mock-data-listsource';

@Component({
  selector: 'list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit, OnDestroy {

  constructor(private mockDataUpdateHandler : MockDataUpdateHandler) { }

  selectedRowIndex: number = -1;

  displayedColumns = ['fundName', 'pnl', 'cumulativePnl', 'return'];
  dataSource : MockDataListSource | null;

  @ViewChild(MatSort) sort : MatSort;

  version = VERSION;

  ngOnInit() {
    // set up subscription
    this.mockDataUpdateHandler.startSubscription();
    this.dataSource = new MockDataListSource(this.mockDataUpdateHandler, this.sort);
  }

  ngOnDestroy() {
    // remove subscription
    this.mockDataUpdateHandler.cancelSubscription();
  }

  highlight(row){
    let data = this.dataSource.getSortedData();
    for (let i = 0; i < data.length; i++){
      if (data[i].fundName == row.fundName){
        this.selectedRowIndex = i;
        break;
      }
    }
  }

  getId() {
    return this.selectedRowIndex;
  }
}
