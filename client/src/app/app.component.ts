import {Component} from '@angular/core';
import {VERSION} from '@angular/material';
import {ListViewComponent} from './list-view/list-view.component';
import {HeaderComponent} from './header/header.component';
import {GraphingComponent} from './graphing/graphing.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Mock Funds';
  version = VERSION;
}
