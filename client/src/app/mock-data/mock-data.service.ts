import * as socketCluster from 'socketcluster-client';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { MockData } from '../models';


@Injectable()
export class MockDataService {

  private sc: socketCluster.Socket = socketCluster.Socket;
  private channel: any;

  private options: any = {
    //hostname: 'http://tickserver.azurewebsites.net',
    port: 4210
  };

  private subject = new Subject<any>();

  constructor() {

    this.sc = socketCluster.connect(this.options);

    this.sc.on('connect', function(status){
      console.log('STATUS: ' + JSON.stringify(status));
    });
    this.sc.on('error', function(data){
      console.log('ERROR - ' + JSON.stringify(data));
    });
    this.sc.on('disconnect', function(){
      console.log('DISCONNECTED');
    });
  }

  public getObservable() : Observable<any>{
        return this.subject.asObservable();
  }

  private formatErrors(error: any) {
     return Observable.throw(error.json());
  }

  public runSubscribeTasks(channelName: string): void {
    this.channel = this.sc.subscribe(channelName);

    this.channel.on('subscribeFail', (err, channel) => {
      console.log(`Failed to subscribe to channel ${channelName}`);
    });

    this.channel.on('subscribe', (channel) => {
      console.log(`Successfully subscribed to channel ${channelName}`);
    });

    this.channel.watch((data) => {
      this.subject.next(data);
    });
  }

  public runUnsubscribeTasks(): void {
    this.channel.unsubscribe();
  }
}
