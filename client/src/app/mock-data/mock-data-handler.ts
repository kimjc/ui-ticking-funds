import {Injectable} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { MockData } from '../models';
import { MockDataService } from './mock-data.service';

@Injectable()
export class MockDataUpdateHandler{
  subscription : Subscription;

  constructor(private mockDataService : MockDataService) {
    this.subscription = this.mockDataService.getObservable().subscribe(data => {
      this.handleUpdate(data);
    });
  }

  dataChange: BehaviorSubject<MockData[]> = new BehaviorSubject<MockData[]>([]);
  get data(): MockData[] { return this.dataChange.value; }

  startSubscription() {
    this.mockDataService.runSubscribeTasks('mockData');
  }

  cancelSubscription() {
    this.mockDataService.runUnsubscribeTasks();
  }

  handleUpdate(data){
    if (data == null || data.length == 0) return;

    this.dataChange.next(data);
    //console.log('fund data: ', data);
  }
}
