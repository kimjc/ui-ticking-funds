import {Injectable} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {MatSort} from '@angular/material';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';

import { MockData } from '../models';
import { MockDataService } from './mock-data.service';
import {MockDataUpdateHandler} from './mock-data-handler';

@Injectable()
export class MockDataListSource extends DataSource<any> {
  constructor(private _database : MockDataUpdateHandler, private _sort : MatSort){
    super();
  }

  connect() : Observable<MockData[]>{

    const displayDataChanges = [
      this._database.dataChange,
      (this._sort || new MatSort()) .sortChange
    ];

    return Observable.merge(...displayDataChanges).map(()=>{
      return this.getSortedData();
    })
  }

  disconnect(){}

  getSortedData() : MockData[]{
    const data = this._database.data.slice();
    if (!(this._sort || {active : false}).active || (this._sort || {direction: ''}).direction == '') { return data;}

    return data.sort((a, b)=>{
      let propertyA : number | Date | string = '';
      let propertyB : number | Date | string = '';

      switch(this._sort.active){
        case 'fundName':
          [propertyA, propertyB] = [a.fundName, b.fundName];
          break;
        case 'pnl':
          [propertyA, propertyB] = [a.pnl, a.pnl];
          break;
        case 'cumulativePnl':
          [propertyA, propertyB] = [a.cumulativePnl, a.cumulativePnl];
          break;
        case 'return':
          [propertyA, propertyB] = [a.return, a.return];
          break;
      }

      let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
    });
  }
}
