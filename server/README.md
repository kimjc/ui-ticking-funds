# Live Fund Ticker: Server

## Instructions
To install this application, clone the repo, navigate to the **server** folder, install npm packages and run npm start. 

```sh
$ npm install 
$ npm start
```

The server is built with socketCluster. It provides an angular service and a data model that will interact with the mock data service. Drop the data model class in your client project and bind to an observable that is returned by the subscription. 

```javascript

// mock-data.model.ts
export interface MockData { 
    fundName: string;
    pnl: number;
    cumulativePnl: number;
    return: number;
}

// mock-data.service.ts
/**
 * This is the service class that you will extending to provide data to your components. 
 **/
import * as socketCluster from 'socketcluster-client';

import { Injectable } from '@angular/core';

@Injectable()
export class MockDataService {

  private sc: socketCluster.Socket = socketCluster.Socket;
  private channel: any;

  private options: any = {
    port: 8000
  };

  constructor() {

    this.sc = socketCluster.connect(this.options);

    this.sc.on('connect', function(status){
      console.log('STATUS: ' + JSON.stringify(status));
    });
    this.sc.on('error', function(data){
      console.log('ERROR - ' + JSON.stringify(data));
    });
    this.sc.on('disconnect', function(){
      console.log('DISCONNECTED');
    });
  }

  public subscribe(channelName: string): void {
    this.channel = this.sc.subscribe(channelName);

    this.channel.on('subscribeFail', (err, channel) => {
      console.log(`Failed to subscribe to channel ${channelName}`);
    });

    this.channel.on('subscribe', (channel) => {
      console.log(`Successfully subscribed to channel ${channelName}`);
    });

    this.channel.watch((data) => {
      console.log('Mock Data Received: ' + JSON.stringify(data));
    });

  }

  public unsubscribe(): void {
    this.channel.unsubscribe();
  }

}

```
