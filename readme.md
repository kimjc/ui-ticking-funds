# Live Fund Ticker

## Overview
An example project of displaying live ticking data in Angular 4.

### Includes:
- Live server written in node.js to provide mock real-time data
- UI written in Angular Material to show live data in grid view
- Plotly chart showing data in real-time
